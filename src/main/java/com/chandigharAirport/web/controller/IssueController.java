package com.chandigharAirport.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chandigharAirport.services.IssueService;
import com.chandigharAirport.web.jsonview.Views;
import com.chandigharAirport.web.model.Issue;
import com.chandigharAirport.web.model.IssueResponseBody;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class IssueController {

	List<Issue> issues;

	Logger logger = Logger.getLogger(IssueController.class.getName());

	@Autowired
	IssueService service;
	
	/**
	 * 
	 * @param Issue
	 * @return SUCCESS / FAILURE / USER_ALREADY_EXSISTS
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/issue/api/register")
	public IssueResponseBody registerNewIssue(@RequestBody Issue newIssue) {
		logger.log(Level.INFO, "Registered Issue: "+newIssue);
		IssueResponseBody result = new IssueResponseBody();
		result.setCode("200");
		service.saveIssue(newIssue);
		return result;

	}

	
	/**
	 * 
	 * @param Issue
	 * @return EMAIL_SENT / EMAIL_NOT_FOUND
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/issue/api/viewIssue")
	public IssueResponseBody viewProfile(@RequestBody Issue issue) {
		logger.log(Level.INFO, "ViewProfile Issue: "+issue);
		
		IssueResponseBody result = new IssueResponseBody();
		result.setCode("200");
//		Issue issueView = issueService.viewProfile(issue);
		Issue issueView = service.findIssueById(issue.getId());
		issues = new ArrayList<Issue>();
		issues.add(issueView);
		result.setResult(issues);

		return result;
	}
	
	
	/**
	 * 
	 * @param Issue
	 * @return SUCCESS / FAILED
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/issue/api/editIssue")
	public IssueResponseBody editProfile(@RequestBody Issue issue) {
		logger.log(Level.INFO, "editProfile Issue: "+issue);
		
		IssueResponseBody result = new IssueResponseBody();
		result.setCode("200");
		service.updateIssue(issue);
//		String issueCreatedMessage = issueService.editProfile(issue);
		String issueCreatedMessage = "SUCCESS";
		result.setMsg(issueCreatedMessage);
		return result;
	}
	
	
	


}
