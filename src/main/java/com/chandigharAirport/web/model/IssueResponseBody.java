package com.chandigharAirport.web.model;

import java.util.List;

import com.chandigharAirport.web.jsonview.Views;
import com.fasterxml.jackson.annotation.JsonView;

public class IssueResponseBody {

	@JsonView(Views.Public.class)
	String msg;
	@JsonView(Views.Public.class)
	String code;
	@JsonView(Views.Public.class)
	List<Issue> result;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<Issue> getResult() {
		return result;
	}

	public void setResult(List<Issue> result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "AjaxResponseResult [msg=" + msg + ", code=" + code + ", result=" + result + "]";
	}

}
