package com.chandigharAirport.dao;

import java.util.List;

import com.chandigharAirport.web.model.Issue;
 
 
public interface IssueDao {
 
    Issue findById(int id);
 
    void saveIssue(Issue Issue);
     
    void deleteIssueById(int ssn);
     
    List<Issue> findAllIssues();
    
    void updateIssue(Issue issue);

 
}