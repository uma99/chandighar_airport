package com.chandigharAirport.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.chandigharAirport.web.model.Issue;
 
 
@Repository("IssueDao")
public class IssueDaoImpl extends AbstractDao<Integer, Issue> implements IssueDao {
 
    public Issue findById(int id) {
        return getByKey(id);
    }
 
    public void saveIssue(Issue Issue) {
        persist(Issue);
    }
    
    public void updateIssue(Issue Issue) {
    	update(Issue);
    }
 
    public void deleteIssueById(int issueId) {
        Query query = getSession().createSQLQuery("delete from Issue where id = :id");
        query.setInteger("Id", issueId);
        query.executeUpdate();
    }
 
    @SuppressWarnings("unchecked")
    public List<Issue> findAllIssues() {
        Criteria criteria = createEntityCriteria();
        return (List<Issue>) criteria.list();
    }

 


}