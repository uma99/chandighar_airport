package com.chandigharAirport.services;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.chandigharAirport.dao.IssueDao;
import com.chandigharAirport.web.model.Issue;

@Service("issueService")
@Transactional
public class IssueServiceImpl implements IssueService {

	@Autowired
	private IssueDao dao;

	@Autowired
	MessageSource messageSource;

	public Issue findById(int id) {
		return dao.findById(id);
	}

	public void saveIssue(Issue Issue) {
		dao.saveIssue(Issue);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate
	 * update explicitly. Just fetch the entity from db and update it with
	 * proper values within transaction. It will be updated in db once
	 * transaction ends.
	 */
	public void updateIssue(Issue issue) {
		// Issue entity = dao.findById(Issue.getId());
		Issue entity = dao.findById(issue.getId());
		System.out.println(" to be udpated Issue s:" + entity);
		if (entity != null) {
			int tempId = entity.getId();
			issue = entity;
			issue.setId(tempId);
			
		}
		dao.updateIssue(issue);
	}

	
	public void deleteIssueById(int id) {
		dao.deleteIssueById(id);
	}

	public List<Issue> findAllIssues() {
		return dao.findAllIssues();
	}

	public Issue findIssueById(int id) {
		return dao.findById(id);
	}



}