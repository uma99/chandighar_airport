package com.chandigharAirport.services;

import java.util.List;

import com.chandigharAirport.web.model.Issue;

public interface IssueService {


	Issue findById(int id);

	void saveIssue(Issue issue);

	void updateIssue(Issue issue);

	void deleteIssueById(int ssn);

	List<Issue> findAllIssues();

	Issue findIssueById(int ssn);

}
